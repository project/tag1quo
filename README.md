## Overview

This module sends information about your website to Tag1 Consulting as part of
their Quo offering. It tracks which modules and themes you have in your
codebase, watching for Drupal LTS notifications and upstream releases to ensure
that any applicable fixes are available to your site in a timely manner. With
this module properly installed and configured, Tag1 will notify you whenever
security patches and patches should be applied.

Data is securely sent to Tag1. This requires that [cron](https://www.drupal.org/cron)
is configured on your website (<>), and that [OpenSSL](http://php.net/manual/en/openssl.installation.php)
support for PHP is properly installed.

For more information, visit: <https://quo.tag1consulting.com>.

## Installation

Repeat these steps for all Drupal websites you've paid for the Quo service
from Tag1 Consulting. To purchase support for additional websites, contact
your reseller or email support@tag1consulting.com.

1. Install the module.
   Extract the provided tag1quo.tar.gz compressed tarball into the appropriate
   modules directory (for example: sites/all/modules).

2. Enable the module.
   Visit 'Administration > Modules' at admin/modules and enable the 'Tag1 Quo'
   module.

3. Configure the module. Visit
   `Administration > Site configuration > Development > Tag1 Quo` at
   `/admin/config/development/tag1quo` (`admin/settings/tag1quo` for Drupal 6) and enter the token provided by Tag1
   Consulting. Then click `Save configuration`.

## Manual overrides via settings.php

There are various settings and configuration you can manually set in your `settings.php` file. Depending on which
version of Drupal this module is installed on, the code may be a little different.

### Drupal 6/7

```php
$conf['tag1quo_enabled'] = TRUE;

$conf['tag1quo_api_token'] = 'YOUR_API_TOKEN';

$conf['tag1quo_siteid'] = 'YOUR_SITEID';

$conf['tag1quo_debug_enabled'] = TRUE;

$conf['tag1quo_curl_enabled'] = FALSE;
$conf['tag1quo_curl_options'] = array();

// For cURL options, the options array requires a key matching a `CURLOPT_` prefixed
// PHP constant and the expected value for that option. For example:
$conf['tag1quo_curl_options'] = array(
  CURLOPT_CONNECTTIMEOUT => 120,
);

// If you installed an older version of this module and already have some $conf
// variables in your settings.php file, you may need to upgrade them.
//
// (existing)              => (replace with)
// $conf['tag1quo_report'] => $conf['tag1quo_enabled']
// $conf['tag1quo_debug']  => $conf['tag1quo_debug_enabled']
// $conf['tag1quo_token']  => $conf['tag1quo_api_token']
// $conf['tag1quo_curl']   => $conf['tag1quo_curl_enabled']
// $conf['tag1quo_curl_*'] => $conf['tag1quo_curl_options'] (put each cURL variable value inside the new options array)
```

### Drupal 8+
```php
$config['tag1quo.settings']['enabled'] = TRUE;

$config['tag1quo.settings']['api']['token'] = 'YOUR_API_TOKEN';

$config['tag1quo.settings']['siteid'] = 'YOUR_SITEID';

$config['tag1quo.settings']['debug']['enabled'] = TRUE;

$config['tag1quo.settings']['curl']['enabled'] = TRUE;
$config['tag1quo.settings']['curl']['options'] = [];

// For cURL options, the options array requires a key matching a `CURLOPT_` prefixed
// PHP constant and the expected value for that option. For example:
$config['tag1quo.settings']['curl']['options'] = [
  CURLOPT_CONNECTTIMEOUT => 120,
];

// If you installed an older version of this module and already have some $config
// or $setting values in your settings.php file, you may need to upgrade them.
//
// (existing)                                    => (replace with)
// $config['tag1quo.settings']['tag1quo_report'] => $config['tag1quo.settings']['enabled']
// $config['tag1quo.settings']['tag1quo_debug']  => $config['tag1quo.settings']['debug']['enabled']
// $config['tag1quo.settings']['tag1quo_token']  => $config['tag1quo.settings']['api']['token']
```

## Troubleshooting

Be sure that you configured your Token correctly (step #3 above). If you see an
error when configuring the token, visit `Recent log entries` (`/admin/reports/dblog`)
and look for messages from the `tag1quo` module. If there's not enough
information in the logs, go to the tag1quo configuration page
(`/admin/config/development/tag1quo`) and enable `Debug logging` in the
`Advanced` section. Then try to save the token again.  Finally, review the
`Recent log entries` once again.

If the problem is not obvious, email [support@tag1consulting.com](mailto:support@tag1consulting.com)
for help.
