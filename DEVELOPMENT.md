## Overview

This project follows the [adapter pattern](https://refactoring.guru/design-patterns/adapter/php/example) to share the
same codebase across all major Drupal versions. This decision was made to help ensure consistency across the codebase,
improve developer experience (DX), and reduce potential human mistakes when back porting similar functionality to older
Drupal versions that lack support for newer architecture and APIs.

## Merge/Pull Requests

Due to this project sharing the same codebase between all branches, all merge/pull requests should be made against the
`HEAD` branch of the project. When a release is to be made, the project maintainers will merge all commits from `HEAD`
to the other subsequent branches; you do not need to create multiple merge/pull requests for the other branches.

## Adapters

All base adapters should extend from `Drupal\tag1quo\VersionedClass` and follow the following naming convention,
`ClassName[N]` (where, `N` is an optional major Drupal version). The `static::createVersionedStaticInstance()` method
can then be invoked to choose the correct class based on the major Drupal version detected. It's common practice in
Drupal to create a static `create()` method and this is where you can invoke this method.

> **Note:** You should never directly instantiate or create a version specific class in code, always invoke the base
> class name directly. For example, do not invoke `MyClass8::create()`; instead invoke `MyClass::create()`. This will
> automatically choose the correct class based on the currently installed major Drupal version.

Base classes should start with the lowest API implementation and include common functionality/helper methods.

```php
<?php

namespace Drupal\tag1quo\Adapter\MyClass;

use Drupal\tag1quo\VersionedClass;

class MyClass extends VersionedClass {
  /**
   * Creat new instance based on the major Drupal version detected. 
   *
   * @return static
   */
  public static function create() {
    return static::createVersionedStaticInstance();
  }
  
  public function getSomething() {
    // Drupal 6 and Drupal 7 versions will likely use procedural functions.
    return \some_procedural_function();
  }
}
```

Once an API has changed due to a major version bump (Drupal 8+), a new versioned class will be needed to implement
the newer implementation approach.

```php
<?php

namespace Drupal\tag1quo\Adapter\MyClass;

class MyClass8 extends MyClass {
  /**
   * @var ServiceInterface 
   */
  protected $aService;

  public function __construct(ServiceInterface $aService) {
    $this->aService = $aService;
  }

  public function getSomething() {
    // Drupal 8+ will likely be using a method on a service.
    return $this->aService->getSomething();
  }
}
```

## Core

This project contains a `Core` class that acts as a sort of pseudo "service container". It allows you to access common
functionality across the project.

### Config

This project follows the newer Drupal 8+ Config API. For Drupal 6 and 7 versions, config names are automatically
converted into variable prefixes (see `\Drupal\tag1quo\Adapter\Config\Config6::$prefixes`) and subsequent paths (dots)
are converted into underscores. For example, when retrieving a config value, always use the Drupal 8+ naming/path
convention:

```php
<?php

use Drupal\tag1quo\Adapter\Core\Core;

Core::create()->config('tag1quo.settings')->get('api.token');

// In Drupal 6/7, the Config class will convert this into a variable name and it would be the same as if you invoked:
\variable_get('tag1quo_api_token'); 
```

## Manual overrides via settings.php

For development purposes various internal settings can be manually overridden in your `settings.php` file. Depending on which
version of Drupal this module is installed on, the code may be a little different.

### Drupal 6/7

```php
$conf['tag1quo_api_server'] = 'https://quo.tag1consulting.com';

$conf['tag1quo_debug_xdebug_session'] = 'PHPSTORM';

// To avoid API rate limits set the heartbeat timestamp to 0.
$conf['tag1quo_heartbeat_timestamp'] = 0;

// The timestamp when the tag1quo module was enabled.
$conf['tag1quo_enable_timestamp'] = 0;

// If you installed an older version of this module and already have some $conf
// variables in your settings.php file, you may need to upgrade them.
//
// (existing)              => (replace with)
// $conf['tag1quo_url']    => $conf['tag1quo_api_server']
```

### Drupal 8+
```php
$config['tag1quo.settings']['api']['server'] = 'https://quo.tag1consulting.com';
$config['tag1quo.settings']['debug']['xdebug']['session'] = 'PHPSTORM';

// If you installed an older version of this module and already have some $config
// or $setting values in your settings.php file, you may need to upgrade them.
//
// (existing)                                    => (replace with)
// $settings['tag1quo_url']                      => $config['tag1quo.settings']['api']['server']
```
