<?php

namespace Drupal\Tests\tag1quo\Functional;

use Drupal\Core\Url;
use Drupal\tag1quo_test\Controller\Tag1QuoTestController;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Tag1Quo user interface.
 *
 * @group tag1quo
 */
class UiTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['tag1quo', 'tag1quo_test', 'block'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $settings['config']['tag1quo.settings']['api']['server'] = (object) [
      'value' => getenv('TEST_API_SERVER') ? 'http://' . getenv('TEST_API_SERVER') . '/tag1quo-mock' : Url::fromUri('internal:/tag1quo-mock')->setAbsolute()->toString(),
      'required' => TRUE,
    ];
    $settings['config']['tag1quo.settings']['api']['token'] = (object) [
      'value' => Tag1QuoTestController::TOKEN,
      'required' => TRUE,
    ];
    $this->writeSettings($settings);
    $this->drupalPlaceBlock('local_tasks_block');
    $permissions = ['administer tag1quo', 'access administration pages'];
    $this->drupalLogin($this->drupalCreateUser($permissions));
  }

  /**
   * Tests configuration values are saved.
   */
  public function testConfigurationForm() {
    $this->drupalGet('admin/config/development');
    $this->assertSession()->pageTextContains('Configure your website to talk to the Tag1 Quo service.');
    $this->clickLink('Tag1 Quo');
    $this->assertSession()->addressEquals('admin/config/development/tag1quo');
    $this->assertSession()->fieldValueEquals('api[token]', Tag1QuoTestController::TOKEN);
    $this->assertSession()->checkboxNotChecked('debug[enabled]');
    $this->assertSession()->pageTextContains('Tag1 Quo Configuration');
    $site_id = \Drupal::config('tag1quo.settings')->get('siteid');
    $this->assertSession()->pageTextContains('Site identifier');
    $this->assertSession()->pageTextContains($site_id);

    // Assert the configuration form cannot be saved without token.
    $this->submitForm(['api[token]' => ''], t('Save configuration'));
    $this->assertSession()->pageTextContainsOnce('Token field is required.');

    // Assert the configuration form can be updated.
    $this->submitForm(['debug[enabled]' => TRUE, 'api[token]' => '123'], t('Save configuration'));
    $this->assertSession()->checkboxChecked('debug[enabled]');
    $this->assertSession()->pageTextNotContains('Token field is required.');
    $this->assertSession()->pageTextContainsOnce('No data was sent to Tag1 Quo. The API Token is invalid. It must be lower-cased alphanumeric string that is 32 characters in length.');

    // Assert the module connects to the mocked API client.
    $this->submitForm(['api[token]' => Tag1QuoTestController::TOKEN], t('Save configuration'));
    $this->assertSession()->pageTextContains('Successfully validated token and communicated with Tag1 Quo Server.');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    // The site identifier has not been changed.
    $this->assertSession()->pageTextContains($site_id);

    // Assert the site identifier is cleared if the module is uninstalled.
    \Drupal::service('module_installer')->uninstall(['tag1quo']);
    $state = \Drupal::state();
    $state->resetCache();
    $this->assertNull($state->get('tag1quo_enable_timestamp'));
    $this->assertNull($state->get('tag1quo_heartbeat_timestamp'));
  }

  /**
   * Tests the review page is displayed correctly.
   */
  public function testReviewPage() {
    $this->drupalGet('admin/config/development/tag1quo');
    $this->clickLink('Review');
    $session = $this->assertSession();
    $session->addressEquals('admin/config/development/tag1quo/review');
    $session->statusCodeEquals(200);
    $session->pageTextContains('Name');
    $session->pageTextContains('Type');
    $session->pageTextContains('Version');
    $session->pageTextContains('Schema version');
    $session->pageTextContains('Enabled');
    $session->pageTextContains('Filename');

    self::assertNotEmpty($this->xpath('//td[normalize-space(text())="tag1quo"]'));
    self::assertNotEmpty($this->xpath('//td[normalize-space(text())="tag1quo_test"]'));
    $this->assertSession()->pageTextContains('modules/contrib/tag1quo');
  }

  /**
   * Tests the status page is displayed correctly.
   */
  public function testStatusPage() {
    $this->drupalGet('admin/config/development/tag1quo');
    $this->clickLink('Status');
    $session = $this->assertSession();
    $session->addressEquals('admin/config/development/tag1quo/status');
    $session->statusCodeEquals(200);
    $session->pageTextContains('No recent heartbeat has been sent to Tag1 Quo. Verify the settings on the configuration page.');
    $session->pageTextContains('Last Heartbeat');
    $session->pageTextContains('Next Heartbeat');
    $this->clickLink('Send manually');
    $this->assertSession()->pageTextContains('Report sent to Tag1 Quo.');
  }

}
