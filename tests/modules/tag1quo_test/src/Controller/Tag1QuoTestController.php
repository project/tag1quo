<?php

namespace Drupal\tag1quo_test\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns mocked responses for Tag1Quo API.
 */
class Tag1QuoTestController {

  /**
   * Valid test token.
   */
  const TOKEN = 'validtoken0123456789abcdefghijkl';

  /**
   * Mock service to validate token.
   */
  public function validateToken(Request $request) {
    $payload = gzdecode($request->getContent());
    $data = Json::decode($payload);
    if (!empty($data['token']) && $data['token'] === static::TOKEN) {
      return new JsonResponse([
        'user' => [
          'id' => \Drupal::currentUser()->id(),
          'name' => \Drupal::currentUser()->getDisplayName(),
        ],
      ]);
    }

    return new JsonResponse(['message' => 'Invalid token'], 403);
  }

  /**
   * Mock service to respond to site resource request.
   */
  public function siteResource() {
    return new Response('', 202);
  }

}
