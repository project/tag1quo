<?php

namespace Drupal\tag1quo\Adapter\Extension;

/**
 * Class Extension9.
 *
 * @internal This class is subject to change.
 *
 * @property \Drupal\Core\Extension\Extension $extension
 */
class Extension9 extends Extension {

  protected $infoExtension = '.info.yml';

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected static $schemaStore;

  /**
   * {@inheritdoc}
   */
  public function __construct($name, $extension) {
    /** @var \Drupal\Core\Extension\Extension $extension */
    $this->extension = $extension;
    $this->name = $name;
    $this->type = $extension->getType();
    $this->path = $this->core()->getPath($this->type, $this->name);
    $this->info = serialize($this->parseInfo($extension->info));
    $this->infoComments = $this->parseInfoComments();
    $this->schema_version = $this->schemaStore()->get($name);
    $this->filename = $extension->getExtensionPathname();
    $this->status = $extension->status;
  }

  /**
   * Retrieves the schema store.
   *
   * @return \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected function schemaStore() {
    if (static::$schemaStore === NULL) {
      static::$schemaStore = \Drupal::keyValue('system.schema');
    }
    return static::$schemaStore;
  }

}
