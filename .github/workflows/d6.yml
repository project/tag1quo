name: Tests Drupal 6
on:
  push:
    branches: [ "3.0.x", "6.x-2.x" ]
  pull_request:
    branches: [ "3.0.x", "6.x-2.x" ]
jobs:
  tests:
    name: Drupal ${{ matrix.drupal-core }} PHP ${{ matrix.php-version }}
    runs-on: ubuntu-16.04
    env:
      EXTENSIONS: mbstring, xml, gd, json, curl, pdo_mysql, zlib
      KEY: cache-${{ matrix.drupal-core }}-${{ matrix.php-version }}
    strategy:
      matrix:
        php-version: [ 5.3, 5.4 ]
        drupal-core: [ 6 ]
    steps:
      - name: Cancel Previous Runs
        uses: styfle/cancel-workflow-action@0.8.0
        with:
          access_token: ${{ secrets.GITHUB_TOKEN }}

      - name: Find latest Drupal ${{ matrix.drupal-core }} tag
        uses: oprypin/find-latest-tag@v1
        with:
          repository: drupal/drupal
          prefix: ${{ matrix.drupal-core }}
        id: drupal-version

      - name: Checkout Drupal ${{ steps.drupal-version.outputs.tag }}
        uses: actions/checkout@v2
        with:
          repository: drupal/drupal
          ref: ${{ steps.drupal-version.outputs.tag }}

      - name: Checkout Tag1Quo module
        uses: actions/checkout@v2
        with:
          path: modules/tag1quo

      - name: Setup PHP extension cache
        id: extcache
        uses: shivammathur/cache-extensions@v1
        with:
          php-version: ${{ matrix.php-version }}
          extensions: ${{ env.EXTENSIONS }}
          key: ${{ env.KEY }}

      - name: Cache PHP extensions
        uses: actions/cache@v2
        with:
          path: ${{ steps.extcache.outputs.dir }}
          key: ${{ steps.extcache.outputs.key }}
          restore-keys: ${{ steps.extcache.outputs.key }}

      - name: Setup PHP, with composer and extensions
        uses: shivammathur/setup-php@v2
        with:
          php-version: ${{ matrix.php-version }}
          # Disable Xdebug for better performance.
          coverage: none
          extensions: ${{ env.EXTENSIONS }}
          ini-values: sendmail_path=/bin/true,display_errors=true,error_reporting=E_ALL

      - name: Setup problem matchers for PHP
        run: echo "::add-matcher::${{runner.tool_cache}}/php.json"

      - name: Setup Drush 7
        run: composer global require "drush/drush:7.*"

      - name: Setup custom host
        run: sudo echo "127.0.0.1 tag1quo.test" | sudo tee -a /etc/hosts

      - name: Setup Apache
        run: |
          cat >> tag1quo.test.conf << EOL
            <VirtualHost *:80>
              ServerName tag1quo.test
              ServerAdmin admin@tag1quo.test
              ServerAlias tag1quo.test
              DocumentRoot $(pwd)
              ErrorLog ${APACHE_LOG_DIR}/error.log
              CustomLog ${APACHE_LOG_DIR}/access.log combined

              <Directory $(pwd)>
                Options Indexes FollowSymLinks
                AllowOverride All
                Require all granted
              </Directory>
            </VirtualHost>
          EOL
          sudo mv tag1quo.test.conf /etc/apache2/sites-available/
          sudo a2ensite tag1quo.test.conf
          sudo a2dismod mpm_event
          sudo a2enmod rewrite mpm_prefork
          sudo apache2ctl configtest
          sudo service apache2 restart

      - name: Start MySQL
        run: sudo systemctl start mysql.service

      - name: Install Drupal ${{ steps.drupal-version.outputs.tag }}
        run: drush si -y --db-url=mysqli://root:root@127.0.0.1:3306/db

      - name: Replace info file
        run: |
          cp modules/tag1quo/tag1quo.info.d${{ matrix.drupal-core }} modules/tag1quo/tag1quo.info
          cp modules/tag1quo/tests/modules/tag1quo_test/tag1quo_test.info.d${{ matrix.drupal-core }} modules/tag1quo/tests/modules/tag1quo_test/tag1quo_test.info

      # Apply core patches and download Simpletest for Drupal 6.
      - name: Setup tests
        run: |
          drush dl simpletest-2.x --destination=modules
          patch -p0 < modules/simpletest/D6-core-simpletest.patch
          curl https://www.drupal.org/files/issues/simpletest_drupal.js_.patch | patch -p0
          cp modules/simpletest/run-tests.sh scripts/

      - name: Enable Simpletest
        run: drush pm-enable simpletest -y

      # run-tests.sh doesn't return an exit code if tests fail. Search for red fail string and fail this step if found.
      - name: Run Simpletest
        run: |
          php scripts/run-tests.sh --url http://tag1quo.test --php $(which php) --color --verbose --file modules/tag1quo/tests/tag1quo.test 2>&1 | tee test_result.log
          if grep -q "\[31mFail\|\[33mException" test_result.log; then exit 1; else exit 0; fi

      - name: Upload test results
        uses: actions/upload-artifact@v2
        if: ${{ always() }}
        with:
          name: artifacts
          path: sites/default/files/simpletest/**/*
